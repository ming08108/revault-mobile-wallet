package com.example.ming.coldwallet;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import com.google.bitcoin.core.Address;
import com.google.bitcoin.core.ECKey;

/**
 * Created by ming on 8/11/2014.
 */
public class KeyOptionsDialog extends DialogFragment {



    CharSequence[] options = {"Sign", "Show Address QR", "Show Private Key"};

    final int SIGN = 0;
    final int SHOW_ADDRESS = 1;
    final int SHOW_PRIVATE_KEY = 2;

    byte[] privKey;
    byte[] pubKey;

    ECKey ecKey;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle args = getArguments();

        if(args != null){
            privKey = args.getByteArray(MainActivity.EXTRA_PRIV_KEY);
            pubKey = args.getByteArray(MainActivity.EXTRA_PUB_KEY);
            ecKey = new ECKey(privKey, pubKey);
        }



        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle("Choose an Option for: " + ecKey.toAddress(MainActivity.mainNetParams).toString()).setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent;
                switch (which){

                    case SIGN:
                        intent = new Intent(getActivity(), SignActivity.class);
                        intent.putExtra(MainActivity.EXTRA_PRIV_KEY, privKey);
                        intent.putExtra(MainActivity.EXTRA_PUB_KEY, pubKey);
                        startActivity(intent);
                        break;
                    case SHOW_ADDRESS:
                        intent = new Intent(getActivity(), QRActivity.class);
                        intent.putExtra(MainActivity.EXTRA_MESSAGE, ecKey.toAddress(MainActivity.mainNetParams).toString());
                        startActivity(intent);
                        break;
                    case SHOW_PRIVATE_KEY:
                        intent = new Intent(getActivity(), QRActivity.class);
                        intent.putExtra(MainActivity.EXTRA_MESSAGE, ecKey.getPrivateKeyEncoded(MainActivity.mainNetParams).toString());
                        startActivity(intent);
                        break;
                    default:
                        break;
                }

            }
        });

        return builder.create();

    }

    static KeyOptionsDialog newInstance(byte[] privKey, byte[] pubKey) {
        KeyOptionsDialog f = new KeyOptionsDialog();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putByteArray(MainActivity.EXTRA_PRIV_KEY, privKey);
        args.putByteArray(MainActivity.EXTRA_PUB_KEY, pubKey);
        f.setArguments(args);

        return f;
    }

    static KeyOptionsDialog newInstance(String addr) {
        KeyOptionsDialog f = new KeyOptionsDialog();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putString("addr", addr);
        f.setArguments(args);

        return f;
    }
}
