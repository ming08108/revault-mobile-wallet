package com.example.ming.coldwallet;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.example.ming.coldwallet.R;
import com.example.ming.coldwallet.camera.ScanActivity;
import com.google.bitcoin.core.ECKey;
import com.google.bitcoin.core.Sha256Hash;
import com.google.bitcoin.core.Transaction;
import com.google.bitcoin.core.TransactionOutput;
import com.google.bitcoin.crypto.TransactionSignature;
import com.google.bitcoin.params.MainNetParams;
import com.google.bitcoin.script.Script;
import com.google.bitcoin.script.ScriptBuilder;
import com.google.zxing.WriterException;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.google.zxing.qrcode.encoder.ByteMatrix;
import com.google.zxing.qrcode.encoder.Encoder;
import com.google.zxing.qrcode.encoder.QRCode;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public class SignActivity extends ActionBarActivity {

    private static final int REQUEST_CODE_SCAN = 0;

    QRCode qrCode;
    ECKey ecKey;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setContentView(R.layout.activity_sign);
        setTitle("Signed Transaction");

        Intent intent = getIntent();
        byte[] privKey = intent.getByteArrayExtra(MainActivity.EXTRA_PRIV_KEY);
        byte[] pubKey = intent.getByteArrayExtra(MainActivity.EXTRA_PUB_KEY);
        ecKey = new ECKey(privKey, pubKey);

        //start the scan
        startActivityForResult(new Intent(this, ScanActivity.class), REQUEST_CODE_SCAN);




    }

    //handle scan return result
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == REQUEST_CODE_SCAN && resultCode == Activity.RESULT_OK)
        {
            final String input = intent.getStringExtra(ScanActivity.INTENT_EXTRA_RESULT);
            try{
                byte[] rawTX = Util.hexStringToByteArray(input);
                Transaction tx = new Transaction(MainActivity.mainNetParams, rawTX);

                //sign the tx

                //get the previous output script
                Script prevOutScript = ScriptBuilder.createOutputScript(ecKey.toAddress(MainNetParams.get()));

                //loop through the inputs and sign each one
                for(int i = 0; i < tx.getInputs().size(); i++){
                    TransactionSignature signature = signInput(i, prevOutScript, ecKey, tx);
                    Script scriptSig = ScriptBuilder.createInputScript(signature, ecKey);
                    tx.getInput(i).setScriptSig(scriptSig);
                }


                //get human readable tx info
                //Add to alertdialog

                final StringBuilder sb = new StringBuilder();
                sb.append("OUTPUTS\n");
                for(TransactionOutput out: tx.getOutputs()){
                    sb.append(out.getValue() + " satoshis to " + out.getScriptPubKey().getToAddress(MainNetParams.get()) + "\n");
                }

                String serialized = Util.bytesToHex(tx.bitcoinSerialize());

                final ArrayList<String> chunks = new ArrayList<String>();

                int dex = 0;

                int chunkSize = 120;

                Double chunksNum = (Math.ceil(serialized.length()/chunkSize) + 1);

                Log.v("SignActivity", "Number of chunks: " + (Math.ceil(serialized.length()/chunkSize) + 1));

                for (int start = 0; start < serialized.length(); start += chunkSize) {
                    String serializedChunk = serialized.substring(start, Math.min(serialized.length(), start + chunkSize));
                    Log.v("SignActivity", serializedChunk);
                    chunks.add((dex++) + ":" + chunksNum.intValue() + ";" + serializedChunk);
                }

                final ImageView qrCode = (ImageView) findViewById(R.id.activity_sign_qr);

                final TextView indexText = (TextView) findViewById(R.id.activity_sign_index);

                final Button next = (Button) findViewById(R.id.activity_sign_next);
                final Button previous = (Button) findViewById(R.id.activity_sign_prev);
                final Button info = (Button) findViewById(R.id.activity_sign_show_info);

                //Set QR code
                final AtomicInteger index = new AtomicInteger(0);

                if(index.get() + 1 != chunks.size()){
                    try {
                        qrCode.setImageBitmap(Util.generateQr(chunks.get(index.get()), ErrorCorrectionLevel.M));
                    } catch (WriterException e) {
                        e.printStackTrace();
                    }
                    indexText.setText("#" + (index.get() +1));
                }


                next.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(index.get() + 1 != chunks.size()){
                            index.addAndGet(1);
                            try {
                                qrCode.setImageBitmap(Util.generateQr(chunks.get(index.get()), ErrorCorrectionLevel.M));
                            } catch (WriterException e) {
                                e.printStackTrace();
                            }
                            indexText.setText("#" + (index.get() + 1));
                        }
                    }
                });

                previous.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(index.get() > 0){
                            index.addAndGet(-1);
                            try {
                                qrCode.setImageBitmap(Util.generateQr(chunks.get(index.get()), ErrorCorrectionLevel.M));
                            } catch (WriterException e) {
                                e.printStackTrace();
                            }
                            indexText.setText("#" + (index.get() + 1));
                        }
                    }
                });

                info.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(SignActivity.this);
                        builder.setTitle("Transaction Information");
                        builder.setMessage(sb.toString());
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                });


            }
            catch (Exception e){
                e.printStackTrace();
                setContentView(R.layout.activity_sign_fail);
            }

        }
        else if(Activity.RESULT_CANCELED == resultCode){
            setContentView(R.layout.activity_sign_fail);
        }

    }


    public static TransactionSignature signInput(int index, Script prevOutScript, ECKey key, Transaction tx) {
        Sha256Hash hash = tx.hashForSignature(index, prevOutScript, Transaction.SigHash.ALL, false);
        ECKey.ECDSASignature txSig = key.sign(hash);
        return new TransactionSignature(txSig, Transaction.SigHash.ALL, false);

    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.sign, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
