package com.example.ming.coldwallet;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.math.BigInteger;
import java.security.SecureRandom;

/**
 * Created by ming on 8/10/2014.
 */
public class AddKeyDialog extends DialogFragment{
    View dialogView;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle("New Key");
        LayoutInflater inflater = getActivity().getLayoutInflater();
        dialogView = inflater.inflate(R.layout.add_key_dialog, null);
        builder.setView(dialogView);
        ((TextView)dialogView.findViewById(R.id.textView)).setText(Html.fromHtml("Type a random number between [1 - 2<sup>256</sup>] or press random"));


        dialogView.findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SecureRandom sRandom = new SecureRandom();
                byte[] bytes = new byte[32];
                sRandom.nextBytes(bytes);
                BigInteger bInt = new BigInteger(1, bytes);
                ((EditText)dialogView.findViewById(R.id.privKeyBytes)).setText(bInt.toString());
            }
        });

        builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                EditText privKeyView = (EditText)dialogView.findViewById(R.id.privKeyBytes);

                BigInteger privKeyInt = new BigInteger(privKeyView.getText().toString());

                MainActivity.addKey(privKeyInt);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        return builder.create();


    }
}
