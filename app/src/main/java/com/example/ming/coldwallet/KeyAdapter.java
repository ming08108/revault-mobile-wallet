package com.example.ming.coldwallet;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ming.coldwallet.camera.ScanActivity;
import com.google.bitcoin.core.ECKey;
import com.google.bitcoin.core.NetworkParameters;
import com.google.bitcoin.params.MainNetParams;
import com.google.zxing.WriterException;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.google.zxing.qrcode.encoder.Encoder;
import com.google.zxing.qrcode.encoder.QRCode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ming on 8/4/2014.
 */
public class KeyAdapter extends BaseAdapter{

    ArrayList<ECKey> ecKeys;
    Activity mAct;
    public KeyAdapter(Iterable<ECKey> keys, Activity act){

        ecKeys = new ArrayList<ECKey>();

        for (ECKey key : keys) {
            ecKeys.add(key);
        }

        mAct = act;
    }

    @Override
    public int getCount() {
        return ecKeys.size();
    }

    @Override
    public Object getItem(int position) {
        return ecKeys.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //TODO implement convertView
        View addressView = mAct.getLayoutInflater().inflate(R.layout.address_view, parent, false);
        TextView addressText = (TextView)addressView.findViewById(R.id.address);
        final String address = ecKeys.get(position).toAddress(MainNetParams.get()).toString();

        Bitmap qr;
        try{
            qr = Util.generateQr(address, ErrorCorrectionLevel.M);
            ImageView iv = (ImageView)addressView.findViewById(R.id.qrAddress);
            iv.setImageBitmap(qr);
        }
        catch (WriterException e){
            e.printStackTrace();
        }



        addressText.setText(address);

        return addressView;
    }

    public void addKey(ECKey key){
        ecKeys.add(key);
    }
}
