package com.example.ming.coldwallet;

import android.graphics.Bitmap;
import android.util.Log;
import android.widget.ImageView;

import com.google.zxing.WriterException;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.google.zxing.qrcode.encoder.ByteMatrix;
import com.google.zxing.qrcode.encoder.Encoder;
import com.google.zxing.qrcode.encoder.QRCode;

/**
 * Created by ming on 8/10/2014.
 */
public class Util {

    public static Bitmap generateQr(String data, ErrorCorrectionLevel ecLevel) throws WriterException{
        //create a qr code
        QRCode qrCode;

        qrCode = Encoder.encode(data, ecLevel);
        Bitmap qr = Bitmap.createScaledBitmap(byteMatrixToBitmap(qrCode.getMatrix()), 200, 200 , false);

        return qr;
    }


    public static Bitmap byteMatrixToBitmap(ByteMatrix result){
        int WHITE = 0xFFFFFFFF;
        int BLACK = 0xFF000000;
        int width = result.getWidth();
        int height = result.getHeight();
        int[] pixels = new int[width * height];
        // All are 0, or black, by default
        for (int y = 0; y < height; y++) {
            int offset = y * width;
            for (int x = 0; x < width; x++) {
                if(result.get(x,y) == 0) {

                    pixels[offset + x] = WHITE;
                }
                else {
                    pixels[offset + x] = BLACK;
                }

            }
        }

        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return bitmap;
    }

    //convert a hex encoded string to a byte array
    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }

    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
}
