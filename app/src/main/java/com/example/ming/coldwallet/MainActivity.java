package com.example.ming.coldwallet;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.ming.coldwallet.camera.ScanActivity;
import com.google.bitcoin.core.Address;
import com.google.bitcoin.core.AddressFormatException;
import com.google.bitcoin.core.ECKey;
import com.google.bitcoin.core.NetworkParameters;
import com.google.bitcoin.core.Sha256Hash;
import com.google.bitcoin.core.Transaction;
import com.google.bitcoin.core.TransactionInput;
import com.google.bitcoin.core.TransactionOutPoint;
import com.google.bitcoin.core.TransactionOutput;
import com.google.bitcoin.core.Wallet;
import com.google.bitcoin.crypto.TransactionSignature;
import com.google.bitcoin.params.MainNetParams;
import com.google.bitcoin.script.ScriptBuilder;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.integration.android.IntentIntegrator;

import com.google.zxing.integration.android.IntentResult;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.google.zxing.qrcode.encoder.ByteMatrix;
import com.google.zxing.qrcode.encoder.Encoder;
import com.google.zxing.qrcode.encoder.QRCode;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class MainActivity extends ActionBarActivity {

    public final static String EXTRA_MESSAGE = "com.example.ming.coldwallet.TEXT";
    public final static String EXTRA_PRIV_KEY = "com.example.ming.coldwallet.KEY";
    public final static String EXTRA_PUB_KEY = "com.example.ming.coldwallet.PUB";
    private static final int REQUEST_CODE_SCAN = 0;


    static Wallet wallet;
    static KeyAdapter keyAdapter;
    static Activity mAct;
    static NetworkParameters mainNetParams;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mAct = this;
        mainNetParams = MainNetParams.get();


        wallet = loadWallet();

        //Set ListView adapter
        ListView listView = (ListView)findViewById(R.id.listView);
        keyAdapter = new KeyAdapter(wallet.getKeys(), mAct);
        listView.setAdapter(keyAdapter);

        //Sign TX onItemClick
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //start a intent to scan with pub/priv keys as extras
                ECKey ecKey = (ECKey)keyAdapter.getItem(position);

                byte[] privKey = ecKey.getPrivKeyBytes();
                byte[] pubKey = ecKey.getPubKey();

                KeyOptionsDialog dialog = KeyOptionsDialog.newInstance(privKey, pubKey);
                dialog.show(getSupportFragmentManager(), "KeyOptionsDialog");
            }
        });


        }




    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public void printByteArray(byte[] bytes){
        for(byte b : bytes){
            Log.d("qr", String.valueOf(b));
        }
    }







    //save the wallet to disk
    public static void saveWallet() {
        String dirName = "/sdcard/ColdWallet";
        String fileName = "wallet.dat";
        File dir = new File (dirName);
        File actualFile = new File (dir, fileName);

        if(!dir.exists()){
                dir.mkdirs();
        }
        try {
            wallet.saveToFile(actualFile);
        }
        catch (IOException e){
            Log.d("ERROR", "WALLET SAVE ERROR");
            e.printStackTrace();
        }
    }

    //add a ECKey to the wallet and update arrayAdapter
    public static void addKey() {
        ECKey key = new ECKey();
        wallet.addKey(key);
        keyAdapter.addKey(key);
        keyAdapter.notifyDataSetChanged();
        Log.d("ERROR", wallet.getKeys().toString());
    }

    //add a ECKey to the wallet and update arrayAdapter
    public static void addKey(BigInteger privKey) {
        ECKey key = new ECKey(privKey, null);
        wallet.addKey(key);
        keyAdapter.addKey(key);
        keyAdapter.notifyDataSetChanged();
        saveWallet();
        Log.d("ERROR", wallet.getKeys().toString());
    }

    //attempts to load wallet from disk. Returns empty wallet on fail.
    public Wallet loadWallet() {
        try{
            String dirName = "/sdcard/ColdWallet";
            String fileName = "wallet.dat";
            File dir = new File (dirName);
            File actualFile = new File (dir, fileName);
            wallet = Wallet.loadFromFile(actualFile);
        }
        catch (Exception e) {
            wallet = new Wallet(mainNetParams);
            Log.d("ERROR", "wallet load Failed");
            e.printStackTrace();
        }
        return wallet;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_add:
                AddKeyDialog addKeyDialog = new AddKeyDialog();
                addKeyDialog.show(getSupportFragmentManager(), "AddKeyDialog");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
